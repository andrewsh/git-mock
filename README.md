git-mock
========

I needed to quickly generate a bunch of repositories with a specific branch layout and some seemingly real data, so this script was born. It runs a series of git-mock scripts (at the moment directly embedded in the source) looking like this:

    checkout -b upstream/buster
    commit
    checkout -b debian/buster
    commit
    checkout upstream/buster
    commit
    checkout debian/buster
    checkout -b debian/buster-security
    merge -s ours upstream/buster
    commit

Basically, it’s Git commands (without the `git` prefix) and a bit of magic: `commit` command makes all commits add some unique data: when it’s ran on a `debian/*` branch, it creates a new entry in the `debian/changelog` file (creating one if it didn’t exist before) with an incrementing version number; when ran on any other branch, it just writes the commit hash `HEAD` is pointint at and the current branch to a file called `mock`.
